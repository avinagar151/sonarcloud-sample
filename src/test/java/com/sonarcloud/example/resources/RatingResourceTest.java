package com.sonarcloud.example.resources;

import static org.hamcrest.CoreMatchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

@SpringBootTest
@AutoConfigureMockMvc
public class RatingResourceTest {

	@Autowired
	private MockMvc mockMvc;
	
	@Test
	public void getRatingTest() {
		try {
			this.mockMvc.perform(get("/ratingsdata/1")).andDo(print()).andExpect(status().isOk())
				.andExpect(content().string(containsString("{\"movieId\":\"1\",\"rating\":4}")));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Test
	public void getUserRatingsTest() {
		try {
			this.mockMvc.perform(get("/ratingsdata/users/1")).andDo(print()).andExpect(status().isOk())
				.andExpect(content().string(containsString("{\"userRating\":[{\"movieId\":\"1234\",\"rating\":4},{\"movieId\":\"5678\",\"rating\":3}]}")));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}
