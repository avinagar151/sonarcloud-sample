package com.sonarcloud.example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SonarcloudSampleApplication {

	public static void main(String[] args) {
		SpringApplication.run(SonarcloudSampleApplication.class, args);
	}
}
